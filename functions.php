<?php

//update_option( 'siteurl', 'http://comecaramelos.ddns.net' ); update_option( 'home', 'http://comecaramelos.ddns.net' );
//update_option( 'siteurl', 'http://comecaramelos.es' ); update_option( 'home', 'http://comecaramelos.es' );
//update_option( 'siteurl', 'http://test.wp.local' ); update_option( 'home', 'http://test.wp.local' );


/**
 * Overwrite footer info
 *
 * Elimino información por defecto del tema y añado créditos propios
 *
 * @version 1.0.3
 * @since   1.0.1
 */
function comecaramelos_footer_credits()
{
	remove_action( 'oblique_footer', 'oblique_footer_credits' );

	echo 'Creado por <a href="https://comecaramelos.es" target="_blank"><span class="sep">@</span>ComeCaramelos</a>';
}
add_action( 'oblique_footer', 'comecaramelos_footer_credits', 1 );


/**
 * Corrección de microfotmatos
 *
 * Corrige un error en la herramienta de testeo de microformatos de Google en los parámetros
 *
 * @version 1.0.1
 * @since   1.0.1
 */
function oblique_posted_on()
{
	$time_string = '<time class="entry-date published updated" datetime="%1$s"><span class="post-date updated">%2$s</span></time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s"><span class="post-date updated">%4$s</span></time>';
	}

	$time_string = sprintf(
		$time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

		$posted_on = sprintf(
			/* translators: Post date */
			_x( '%s', 'post date', 'oblique' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

	$category = get_the_category();
	if ( $category ) {
		$cat = '<a href="' . esc_url( get_category_link( $category[0]->term_id ) ) . '">' . esc_attr( $category[0]->cat_name ) . '</a>';
	}

	$byline = sprintf(
		/* translators: Post author */
		_x( '%s', 'post author', 'oblique' ),
		'<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '"><span class="vcard author author_name post-author"><span class="fn">' . esc_html( get_the_author() ) . '</span></span></a>'
	);

	if ( ! is_singular() ) {
		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span><span class="cat-link">' . $cat . '</span>';
	} elseif ( ! get_theme_mod( 'meta_singles' ) ) {
		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>';
		if ( 'post' == get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'oblique' ) );
			if ( $categories_list ) {
				/* translators: Categories list */
				printf( '<span class="cat-links">' . __( '%1$s', 'oblique' ) . '</span>', $categories_list );
			}
		}
	}
}


/**
 * Carga un nuevo archivo de para evitar errores con el tema Oblique en Wordpress 5.5
 *
 * @link  https://wordpress.org/support/topic/update-to-wp-5-5-breaks-the-recent-posts-home-page/#post-13248133
 * @since 1.1.0
 */
function _oblique_masonry_fix()
{
	wp_dequeue_script( 'oblique-masonry-init' );
	wp_enqueue_script( 'oblique-masonry-init', get_stylesheet_directory_uri() . '/js/vendor/masonry-init.js', array( 'jquery', 'masonry' ), true );
}
add_action( 'wp_enqueue_scripts', '_oblique_masonry_fix' );