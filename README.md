# ComeCaramelos

Tema hijo de Oblique creado para el website [ComeCaramelos.es](http://comecaramelos.es)
 
## Dependencias
| NOMBRE | VERSION | WP | WEB |
| :--- | :---: | :---: | :---: |
| Oblique | 2.0.7 | [WordPress](https://es.wordpress.org/themes/oblique/) | [Website](https://themeisle.com/themes/oblique/)


## Changelog

### 1.1.0
 * Corrección de _.js_ para evitar errores con el tema __Oblique__ en la versión 5.5 de Wordpress

### 1.0.2
 * Actualización en la programación para los créditos del footer. Ahora es posible actualizar el tema padre sin realizar modificaciones
 * Correcciones de formato en archivo _README.md_

### 1.0.1
 * Corregido error en Google's Search Console en valores de _author_ y _updated_ para microformatos.

### 1.0.0
 * Cambio en los créditos del footer.